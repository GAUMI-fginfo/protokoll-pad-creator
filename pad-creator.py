#!/usr/bin/env python
from __future__ import annotations
from typing import List, Tuple, cast
import requests
import sys
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from jinja2 import Template

from dataclasses import dataclass
from dataclass_wizard import JSONWizard


@dataclass
class Config(JSONWizard):
    class _(JSONWizard.Meta):
        key_transform_with_dump = 'NONE'
        key_transform = 'NONE'
    repeat_days: int | None = None
    preset_pad_url: str | None = None
    start_date: date | None = None
    end_date: date | None = None
    metapad_entry_format_str: str | None = "- [Protokoll {{pad_date.strftime('%Y-%m-%d')}}]({{pad_url}})"
    reverse_metapad_entries: bool = True
    hedgedoc_session_cookie: str | None = None

    def clean_values(self):
        if self.preset_pad_url is not None:
            # Filter # and ? as well as all that follows it out of the URL
            # in case someone copies a URL that contains e.g. ?both
            self.preset_pad_url = self.preset_pad_url.split("#", 1)[0]
            self.preset_pad_url = self.preset_pad_url.split("?", 1)[0]
            # Complete the URL in case someone pasted only the pad ID
            if not self.preset_pad_url.startswith("https://pad.gwdg.de/"):
                self.preset_pad_url = "https://pad.gwdg.de/" + self.preset_pad_url


# Lese config
config_file = sys.argv[1] if len(sys.argv) > 1 else "config.json"
with open(config_file) as f:
    config = cast(Config, Config.from_json(f.read()))
config.clean_values()


def menu_edit_repeat_days():
    repeat_days_str = input("Wiederholungszeitraum in Tagen: ")
    config.repeat_days = int(repeat_days_str)


def menu_edit_preset_pad_url():
    preset_pad = input("Pad-Vorlage URL: ")
    # Filter # and ? as well as all that follows it out of the URL
    # in case someone copies a URL that contains e.g. ?both
    preset_pad = preset_pad.split("#", 1)[0]
    preset_pad = preset_pad.split("?", 1)[0]
    # Complete the URL in case someone pasted only the pad ID
    if not preset_pad:
        raise Exception("Invalide Pad-Vorlage URL.")
    if not preset_pad.startswith("https://pad.gwdg.de/"):
        preset_pad = "https://pad.gwdg.de/" + preset_pad
    config.preset_pad_url = preset_pad


def menu_edit_start_date():
    start_date_str = input("Startdatum (YYYY-MM-DD) [Standard=heute]: ")
    if start_date_str == "":
        start_date = date.today()
    else:
        start_date = datetime.strptime(start_date_str, "%Y-%m-%d").date()
    config.start_date = start_date


def menu_edit_end_date():
    start_date = config.start_date
    if not isinstance(start_date, date):
        # raise Exception("Startdatum muss zuerst korrekt gesetzt werden.")
        print("Startdatum muss zuerst korrekt gesetzt werden.")
        menu_edit_start_date()
    start_date = config.start_date
    assert isinstance(start_date, date)
    end_date_str = input("Enddatum (YYYY-MM-DD) [Standard=Startdatum + 6 Monate]: ")
    if end_date_str == "":
        end_date = start_date + relativedelta(months=6)
    else:
        end_date = datetime.strptime(end_date_str, "%Y-%m-%d").date()
    if end_date < start_date:
        raise Exception("Das Enddatum liegt nicht nach dem Startdatum")
    config.end_date = end_date


def menu_edit_metapad_entry_format_str():
    metapad_entry_format_str = input("Formattierung für Einträge im Metapad: ")
    if not metapad_entry_format_str:
        raise Exception("Wert darf nicht leer sein.")
    config.metapad_entry_format_str = metapad_entry_format_str


def menu_edit_reverse_metapad_entries():
    config.reverse_metapad_entries = not config.reverse_metapad_entries


def menu_edit_hedgedoc_session_cookie():
    hedgedoc_session_cookie = input("HedgeDoc Session Cookie (connect.sid): ")
    if not hedgedoc_session_cookie:
        raise Exception("Wert darf nicht leer sein.")
    config.hedgedoc_session_cookie = hedgedoc_session_cookie


def menu():
    config_keys = list(config.to_dict().keys())
    while True:
        config.clean_values()
        print()
        print("Einstellungen:")
        autoselected: None | int = None
        for index, key in enumerate(config_keys):
            val = config.to_dict()[key]
            print(f"{index}: {key}={val}")
            if val is None and autoselected is None:
                autoselected = index
        if autoselected is not None:
            index = autoselected
        else:
            index = input(f"Welche Einstellung möchtest du noch verändern? [0-{len(config.to_dict()) - 1} oder Enter zum Fortfahren]: ")
        if index == "":
            return
        try:
            index = int(index)
            key = config_keys[index]
            print()
            print(f"[{key}]")
            eval(f"menu_edit_{key}()")  # böse böse böse, aber funktioniert
        except Exception as e:
            print("FEHLER:", e)


# ask user for unset config values and if they want to edit the config further
menu()

# validate config values are not None (this is mostly for the LSP to shutup)
assert config.preset_pad_url is not None
assert config.start_date is not None
assert config.end_date is not None
assert config.repeat_days is not None
assert config.hedgedoc_session_cookie is not None
assert config.metapad_entry_format_str is not None


print("Nutze nun folgende Config:")
print(config)

# download template
preset_response = requests.get(config.preset_pad_url + "/download")
assert preset_response.status_code == 200, "Konnte Vorlagen-Pad nicht abrufen!"
# print("=" * 80)
# print(preset_response.text)
# print("=" * 80)


# determine all pad dates
pad_dates: List[Tuple[date, int]] = []
pad_date = config.start_date
nth_pad_in_month = 0
while pad_date <= config.end_date:
    nth_pad_in_month += 1
    pad_dates.append((pad_date, nth_pad_in_month))
    prev_date = pad_date
    pad_date += timedelta(days=config.repeat_days)
    if prev_date.month != pad_date.month or prev_date.year != pad_date.year:
        nth_pad_in_month = 0

if config.reverse_metapad_entries:
    pad_dates = list(reversed(pad_dates))


metapad_entry_template = Template(config.metapad_entry_format_str)
pad_template = Template(preset_response.text)

log_file = open("log.txt", "a")
print()
print("Erstelle Protokoll Pads...")
print()
for pad_date, nth_pad_in_month in pad_dates:
    context = {
        "config": config,
        "pad_date": pad_date,
        "pad_dates": pad_dates,
        "nth_pad_in_month": nth_pad_in_month,
    }
    pad_contents = pad_template.render(context)
    create_response = requests.post("https://pad.gwdg.de/new", data=pad_contents, cookies={"connect.sid": config.hedgedoc_session_cookie}, headers={"Content-Type": "text/markdown"})
    assert create_response.status_code == 200, f"Fehler beim erstellen vom Pad: {create_response}"

    context["pad_url"] = create_response.url
    if create_response.url.endswith("/"):
        raise Exception(f"Hedgedoc gave us an invalid response url ({create_response.url}) when creating the pad. This indicates that your session cookie may be invalid.")
    metapad_entry = metapad_entry_template.render(context)
    print(metapad_entry)
    log_file.write(metapad_entry + "\n")
    log_file.flush()
log_file.close()

print()
print("Alle Protokoll Pads wurden erfolgreich erstellt.")
print("Bitte trage die obige Liste ins entsprechende Metapad ein.")
print("YIPPPIEEE :3")
