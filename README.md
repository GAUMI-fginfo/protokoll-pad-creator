---
lang: de
---

# Protokoll Pad Creator

Hier kommt vielleicht noch mehr Dokumentation, aber eigentlich müsste es einfach möglich sein das Skript auszuführen.

## Ausführen
```bash
python3 -m venv venv              # venv erstellen
source venv/bin/activate          # venv aktivieren
pip3 install -r requirements.txt  # abhängigkeiten installieren
./pad-creator.py                  # pads erstellen
```
